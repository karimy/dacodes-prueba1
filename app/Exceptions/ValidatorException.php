<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 29/10/18
 * Time: 10:31 PM
 */

namespace App\Exceptions;
use Exception;
use Illuminate\Http\Response;


class ValidatorException extends Exception
{
    /**
     * The status code to use for the response.
     *
     * @var int
     */
    public $status = Response::HTTP_UNPROCESSABLE_ENTITY;

    public $errors = [];
    /**
     * ApiValidationException constructor.
     */
    public function __construct($errors)
    {

        parent::__construct('The given data was invalid.', $this->status);
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

}
