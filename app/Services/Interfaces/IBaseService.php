<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 29/10/18
 * Time: 08:19 PM
 */

namespace App\Services\Interfaces;


use Illuminate\Http\Request;

interface IBaseService
{

    public function index();
    public function find();
    public function get($id);
    public function store(Request $request);
    public function update($id, Request $request);
    public function destroy($id);
    public function create();
    public function edit($id);
    public function storeMultiple(Request $request);
    public function saveFromFile(Request $request);
    public function createWithFile();
    public function download();

}