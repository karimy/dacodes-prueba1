<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 29/10/18
 * Time: 10:14 PM
 */

namespace App\Services\Validators;


use Illuminate\Http\Request;

interface IUrlValidator
{

    public function validateStore(Request $request);
    public function validateSaveFromFile(Request $request);

}