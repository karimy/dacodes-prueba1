<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 29/10/18
 * Time: 10:14 PM
 */

namespace App\Services\Validators;


use App\Exceptions\ValidatorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UrlValidator implements IUrlValidator
{


    public function validateStore(Request $request){
        $attributes = $request->all();

        $rules = [
            "long_url" => ['required', 'string'],
            ];

        $validator = Validator::make($attributes, $rules);


        if($validator->fails()){
            throw new ValidatorException("Validation Errors");
        }

    }

    public function validateSaveFromFile(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            "file" => ['required', 'mimes:txt'],
        ];

        $validator = Validator::make($attributes, $rules);


        if($validator->fails()){
            throw new ValidatorException("Validation Errors");
        }
    }
}