<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 30/10/18
 * Time: 12:29 PM
 */

namespace App\Services\DTO\UrlResponse;


class CreateMultipleUrlResponse
{

    private $urls;

    /**
     * @return array
     */
    public function getUrls(): ?array
    {
        return $this->urls;
    }

    /**
     * @param array $urls
     */
    public function setUrls(?array $urls)
    {
        $this->urls = $urls;
    }




}