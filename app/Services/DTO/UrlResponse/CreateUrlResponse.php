<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 30/10/18
 * Time: 12:02 PM
 */

namespace App\Services\DTO\UrlResponse;


use PhpParser\JsonDecoder;

class CreateUrlResponse extends JsonDecoder
{

    private $long_url;
    private $short_url;

    /**
     * @return mixed
     */
    public function getLongUrl()
    {
        return $this->long_url;
    }

    /**
     * @param mixed $long_url
     */
    public function setLongUrl($long_url): void
    {
        $this->long_url = $long_url;
    }

    /**
     * @return mixed
     */
    public function getShortUrl()
    {
        return $this->short_url;
    }

    /**
     * @param mixed $short_url
     */
    public function setShortUrl($short_url): void
    {
        $this->short_url = $short_url;
    }



}