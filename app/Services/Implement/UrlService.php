<?php
/**
 * Created by PhpStorm.
 * User: karimy
 * Date: 29/10/18
 * Time: 08:17 PM
 */

namespace App\Services\Implement;


use App\Services\DTO\UrlResponse\CreateUrlResponse;
use App\Services\Interfaces\IUrlService;
use App\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UrlService implements IUrlService
{

    public function index()
    {
        $urls = Url::all();
        $server = config('app.endpoint_url');
        return view('url-list', compact("urls", "server"));
    }

    public function find()
    {
        $urls = Url::all();
        return response()->json($urls);
    }

    public function get($id)
    {
        $url = DB::table('urls')->where('short_url', $id)->first();

        return redirect()->to($url->long_url);
    }

    public function store(Request $request)
    {
        $url = $this->storeURL($request->long_url);
        return config('app.endpoint_url').$url->short_url;
    }

    public function update($id, Request $request)
    {
        // TODO: Implement update() method.
    }

    public function destroy($id)
    {

        $url = Url::find($id);
        if($url != null){
            $url->delete();
            return "Eliminado exitosamente";
        }else{
            return "No se encontraron coincidencias";
        }

    }

    private function findByUrl($long_url){

        $url = DB::table('urls')->where('long_url', $long_url)->first();
        return $url;
    }

    public function create()
    {
        //
        return view('url-simple-create');
    }

    public function edit($id)
    {
        // TODO: Implement edit() method.
    }

    public function storeMultiple(Request $request){
        $urlsToCreate = $request->all();

        $urlsShort = array();
        foreach ($urlsToCreate as $url => $longUrl){
            $url = $this->storeURL($longUrl);

            $elementoUrl = (object)[
                "id" => $url->id,
                "url_long" => $url->long_url,
                "short_url" => $url->short_url
            ];
            array_push($urlsShort, $elementoUrl);

        }

        return response()->json($urlsShort);
    }

    public function storeURL($longUrl){

        $url = $this->findByUrl($longUrl);
        //Verificar si ya existe en la BD
        if($url === null){
            $url = new Url();

            $url->long_url  = $longUrl;
            $url->short_url = hash('adler32', $longUrl);

            $url->save();
        }

        return $url;
    }

    public function saveFromFile(Request $request){

        $urls = file($request->file('file'));

        $urlsShort = array();
        foreach ($urls as $url => $url_long) {
            $urlToShort = trim($url_long);
            $url = $this->storeURL($urlToShort);
            $elementoUrl = (object)[
                "id" => $url->id,
                "long_url" => $url->long_url,
                "short_url" => config('app.endpoint_url').$url->short_url
            ];
            array_push($urlsShort, $elementoUrl);
        }

        return response()->json($urlsShort);

    }

    public function createWithFile(){
        return view('url-file-create');

    }

    public function download()
    {
        $urls = Url::all();
        $server = config('app.endpoint_url');

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="urls-server.csv"');

        $fp = fopen('php://output', 'wb');
        //Encabezado Archivo
        $val = explode(",", "Id, Long url, Short url");
        fputcsv($fp, $val);
        foreach ( $urls as $line ) {
            $val = explode(",", $line->id.",".$line->long_url.",".$server.$line->short_url);
            fputcsv($fp, $val);
        }
        fclose($fp);
    }
}