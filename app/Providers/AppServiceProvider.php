<?php

namespace App\Providers;

use App\Services\Implement\UrlService;
use App\Services\Interfaces\IUrlService;
use App\Services\Validators\IUrlValidator;
use App\Services\Validators\UrlValidator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //URL
        $this->app->bind(IUrlService::class,    UrlService::class);
        $this->app->bind(IUrlValidator::class,  UrlValidator::class);

    }
}
