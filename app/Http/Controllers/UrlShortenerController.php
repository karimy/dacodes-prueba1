<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\IUrlService;
use App\Services\Validators\IUrlValidator;
use App\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class UrlShortenerController extends Controller
{

    private $urlService;
    private $urlValidator;

    /**
     * UrlService constructor.
     */
    public function __construct(IUrlService $urlService, IUrlValidator $urlValidator)
    {
        $this->urlService   = $urlService;
        $this->urlValidator = $urlValidator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return $this->urlService->index();
    }

    public function find(Request $request)
    {
        //
        return $this->urlService->find($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return $this->urlService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->urlValidator->validateStore($request);
        return $this->urlService->store($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return $this->urlService->get($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $this->urlService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->urlService->destroy($id);

    }

    public function storeMultiple(Request $request){
        return $this->urlService->storeMultiple($request);
    }

    public function saveFromFile(Request $request){
        $this->urlValidator->validateSaveFromFile($request);
        return $this->urlService->saveFromFile($request);
    }

    public function createWithFile(){
        return $this->urlService->createWithFile();

    }

    public function download(){
        return $this->urlService->download();
    }

}
