<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>URL - Shortener</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">


    </head>
    <body>
        <div class="flex-center position-ref full-height" >
            <div class="content" id="app">
                <div class="title m-b-md m-t-150">
                    URL - Shortener
                </div>

                <div class="links">
                    <a href="{{ url('/api/url/create') }}">Short an URL</a>
                    <a href="{{ url('/api/url/') }}">All urls</a>
                    <a href="{{ url('/api/url/file/create') }}">Generate with .txt</a>
                    {{--<a href="{{ url('/api/url/file/download') }}">Download File</a>--}}
                </div>

            </div>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
