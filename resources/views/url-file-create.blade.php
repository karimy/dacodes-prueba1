<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>URL - Shortener</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content" id="app">
        <div class="menu">
           <ul><a class="btn-menu" href="{{ url('/') }}">Ir al inicio</a></ul>
        </div>

        <h1>Create Short URL with File </h1>

        <create-with-file></create-with-file>



    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
