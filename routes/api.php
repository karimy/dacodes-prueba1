<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/url/find', 'UrlShortenerController@find');
Route::post('/url/multiple', 'UrlShortenerController@storeMultiple');
Route::post('/url/file', 'UrlShortenerController@saveFromFile');
Route::get('/url/file/create', 'UrlShortenerController@createWithFile');
Route::get('/url/file/download', 'UrlShortenerController@download');
Route::resource('/url', 'UrlShortenerController');

