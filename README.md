**INSTRUCCIONES** 
-Instalar composer 
-Instalar laravel via composer 
	composer global require "laravel/installer"
-Clonar repositorio en servidor local 
	git clone git@bitbucket.org:karimy/dacodes-prueba1.git

-Iniciar el servidor 
-Generar api_key 
	php artisan key:generate
-Modificar en archivo .env del repositorio las variables, dependiendo de tu entorno 
	APP_URL=http://localhost 
	APP_URL_WEB = http://localhost/dacodes-prueba1/public 
	APP_ENDPOINT_URL = http://localhost/dacodes-prueba1/public/api/url/ 
	DB_CONNECTION=mysql 
	DB_HOST=127.0.0.1 
	DB_PORT=3306 
	DB_DATABASE=dacodes-prueba1-karimy 
	DB_USERNAME=root 
	DB_PASSWORD=

-Crear una BD con el nombre agregado en DB_DATABASE 
-Ejecutar las migraciones 
	php artisan migrate

-Modificar en app.js en resources las variables dependiendo de tu entorno 
	window.apiUrl = "http://localhost/dacodes-prueba1/public/api/"; 
	window.urlBase = "http://localhost/dacodes-prueba1/public";

-Install node 
	npm install 
-Ejecutar
	npm run dev
	

**EJECUTAR EL PROYECTO**

-Ingresar a la URL principal 
Ejemplo: http://localhost/dacodes-prueba1/public/

**WEB SERVICES** 
-Link colección postman: https://www.getpostman.com/collections/d363a6d8d89ec1bc78e0

En la raiz del proyecto se encuentra un archivo json con la colleccion de postman Dacodes-prueba1-karimy.postman_collection.json

Para utilizar la coleccion es necesario crear un 'enviroment' en postman que cuente con la variable 'api_server' que tenga la url a nuestro proyecto direccionada a api

Ej: "http://localhost/dacodes-prueba1/public/api/"
